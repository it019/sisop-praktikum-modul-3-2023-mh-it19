#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROWS1 9
#define COLS2 9

int *result;
char *transposed[COLS2][ROWS1];

int multiply(int size, int res[], int n)
{
    int carry = 0;
    for (int i = 0; i < size; i++)
    {
        int product = res[i] * n + carry;
        res[i] = product % 10;
        carry = product / 10;
    }

    while (carry)
    {
        res[size] = carry % 10;
        carry = carry / 10;
        size++;
    }

    return size;
}

char *factorialAsString(int n)
{
    int res[1000] = {0};
    int size = 1;
    res[0] = 1;

    for (int i = 2; i <= n; i++)
    {
        size = multiply(size, res, i);
    }

    char *resultStr = (char *)malloc(1001);
    resultStr[0] = '\0';

    for (int i = size - 1; i >= 0; i--)
    {
        char digit[2];
        sprintf(digit, "%d", res[i]);
        strcat(resultStr, digit);
    }

    return resultStr;
}

int main()
{
    key_t key = ftok("belajar.c", 'R');
    int shmid = shmget(key, sizeof(int) * ROWS1 * COLS2, 0666);
    result = (int *)shmat(shmid, (void *)0, 0);

    for (int i = 0; i < ROWS1; i++)
    {
        for (int j = 0; j < COLS2; j++)
        {
            transposed[j][i] = (char *)malloc(1001);
            sprintf(transposed[j][i], "%d", result[i * COLS2 + j]);
            char *factResultStr = factorialAsString(atoi(transposed[j][i]));
            strcpy(transposed[j][i], factResultStr);
            free(factResultStr);
        }
    }

    printf("Hasil transpose matriks:\n");
    for (int i = 0; i < COLS2; i++)
    {
        for (int j = 0; j < ROWS1; j++)
        {
            printf("%s\t", transposed[i][j]);
        }
        printf("\n");
    }

    printf("Hasil faktorial matriks:\n");
    for (int i = 0; i < COLS2; i++)
    {
        for (int j = 0; j < ROWS1; j++)
        {
            printf("%s\t", transposed[i][j]);
        }
        printf("\n");
    }

    shmdt(result);

    for (int i = 0; i < COLS2; i++)
    {
        for (int j = 0; j < ROWS1; j++)
        {
            free(transposed[i][j]);
        }
    }

    return 0;
}