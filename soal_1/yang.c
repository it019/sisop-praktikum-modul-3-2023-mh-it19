#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#define ROWS1 9
#define COLS2 9

int *result;
char *transposed[COLS2][ROWS1];

int multiply(int size, int res[], int n)
{
    int carry = 0;
    for (int i = 0; i < size; i++)
    {
        int product = res[i] * n + carry;
        res[i] = product % 10;
        carry = product / 10;
    }

    while (carry)
    {
        res[size] = carry % 10;
        carry = carry / 10;
        size++;
    }

    return size;
}

char *factorialAsString(int n)
{
    int res[1000] = {0};
    int size = 1;
    res[0] = 1;

    for (int i = 2; i <= n; i++)
    {
        size = multiply(size, res, i);
    }

    char *resultStr = (char *)malloc(1001);
    resultStr[0] = '\0';

    for (int i = size - 1; i >= 0; i--)
    {
        char digit[2];
        sprintf(digit, "%d", res[i]);
        strcat(resultStr, digit);
    }

    return resultStr;
}

void *calculateFactorial(void *data)
{
    int *args = (int *)data;
    int row = args[0];
    int col = args[1];
    char *factStr = transposed[col][row];
    char *factResultStr = factorialAsString(atoi(factStr));
    strcpy(transposed[col][row], factResultStr);
    free(factResultStr);
    pthread_exit(NULL);
}

int main()
{
    key_t key = ftok("belajar.c", 'R');
    int shmid = shmget(key, sizeof(int) * ROWS1 * COLS2, 0666);
    result = (int *)shmat(shmid, (void *)0, 0);

    for (int i = 0; i < ROWS1; i++)
    {
        for (int j = 0; j < COLS2; j++)
        {
            transposed[j][i] = (char *)malloc(1001);
            sprintf(transposed[j][i], "%d", result[i * COLS2 + j]);
        }
    }

    printf("Hasil transpose matriks:\n");
    for (int i = 0; i < COLS2; i++)
    {
        for (int j = 0; j < ROWS1; j++)
        {
            printf("%s\t", transposed[i][j]);
        }
        printf("\n");
    }

    pthread_t threads[COLS2][ROWS1];
    for (int i = 0; i < COLS2; i++)
    {
        for (int j = 0; j < ROWS1; j++)
        {
            int *args = (int *)malloc(2 * sizeof(int));
            args[0] = j;
            args[1] = i;
            pthread_create(&threads[i][j], NULL, calculateFactorial, args);
        }
    }

    for (int i = 0; i < COLS2; i++)
    {
        for (int j = 0; j < ROWS1; j++)
        {
            pthread_join(threads[i][j], NULL);
        }
    }

    printf("Hasil faktorial matriks:\n");
    for (int i = 0; i < COLS2; i++)
    {
        for (int j = 0; j < ROWS1; j++)
        {
            printf("%s\t", transposed[i][j]);
        }
        printf("\n");
    }

    shmdt(result);

    for (int i = 0; i < COLS2; i++)
    {
        for (int j = 0; j < ROWS1; j++)
        {
            free(transposed[i][j]);
        }
    }

    return 0;
}