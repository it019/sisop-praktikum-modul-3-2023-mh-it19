#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROWS1 9
#define COLS1 2
#define ROWS2 2
#define COLS2 9

int main()
{
    int matrix1[ROWS1][COLS1];
    int matrix2[ROWS2][COLS2];
    int result[ROWS1][COLS2];

    srand(time(NULL));
    for (int i = 0; i < ROWS1; i++)
    {
        for (int j = 0; j < COLS1; j++)
        {
            matrix1[i][j] = rand() % 4 + 1;
        }
    }

    for (int i = 0; i < ROWS2; i++)
    {
        for (int j = 0; j < COLS2; j++)
        {
            matrix2[i][j] = rand() % 5 + 1;
        }
    }

    for (int i = 0; i < ROWS1; i++)
    {
        for (int j = 0; j < COLS2; j++)
        {
            result[i][j] = 0;
            for (int k = 0; k < COLS1; k++)
            {
                result[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }

    key_t key = ftok("belajar.c", 'R');
    int shmid = shmget(key, sizeof(int) * ROWS1 * COLS2, 0666 | IPC_CREAT);
    int *shared_memory = (int *)shmat(shmid, NULL, 0);
    for (int i = 0; i < ROWS1; i++)
    {
        for (int j = 0; j < COLS2; j++)
        {
            shared_memory[i * COLS2 + j] = result[i][j];
        }
    }

    printf("Hasil perkalian matriks:\n");
    for (int i = 0; i < ROWS1; i++)
    {
        for (int j = 0; j < COLS2; j++)
        {
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }

    shmdt(shared_memory);

    return 0;
}