#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

#define MAX_FILENAME 256
#define MAX_BUFFER 1024

void toLowerCase(char *str)
{
    for (int i = 0; str[i]; i++)
    {
        str[i] = tolower(str[i]);
    }
}

void parentProcess(char *inputFile, char *option)
{
    FILE *file = fopen(inputFile, "r");
    FILE *output = fopen("thebeatles.txt", "w");

    if (file == NULL)
    {
        printf("Failed to open the input file.\n");
        return;
    }

    if (output == NULL)
    {
        printf("Failed to create the output file.\n");
        return;
    }

    char c;
    while ((c = fgetc(file)) != EOF)
    {
        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == ' ' || c == '\n')
        {
            fputc(c, output);
        }
    }

    fclose(file);
    fclose(output);
}

void childProcessKata(char *inputFile, char *target)
{
    FILE *file = fopen(inputFile, "r");

    if (file == NULL)
    {
        printf("Failed to open the input file.\n");
        return;
    }

    toLowerCase(target);

    char word[100];
    int count = 0;

    while (fscanf(file, "%99[^a-zA-Z]%99[a-zA-Z]", word, word) != EOF)
    {
        toLowerCase(word);

        if (strcmp(word, target) == 0)
        {
            count++;
        }
    }

    fclose(file);

    printf("Kata '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", target, count);
    FILE *logFile = fopen("frekuensi.log", "a");
    if (logFile != NULL)
    {
        time_t now;
        struct tm *tm_info;
        time(&now);
        tm_info = localtime(&now);
        char time_str[20];
        strftime(time_str, 20, "%d/%m/%y %H:%M:%S", tm_info);

        fprintf(logFile, "[%s] [KATA] Kata '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", time_str, target, count);
        fclose(logFile);
    }
}

void childProcessHuruf(char *inputFile, char *target)
{
    FILE *file = fopen(inputFile, "r");

    if (file == NULL)
    {
        printf("Failed to open the input file.\n");
        return;
    }

    char c;
    int count = 0;

    while ((c = fgetc(file)) != EOF)
    {
        if (c == target[0])
        {
            count++;
        }
    }

    fclose(file);

    printf("Huruf '%c' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", target[0], count);
    FILE *logFile = fopen("frekuensi.log", "a");
    if (logFile != NULL)
    {
        time_t now;
        struct tm *tm_info;
        time(&now);
        tm_info = localtime(&now);
        char time_str[20];
        strftime(time_str, 20, "%d/%m/%y %H:%M:%S", tm_info);

        fprintf(logFile, "[%s] [HURUF] Huruf '%c' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", time_str, target[0], count);
        fclose(logFile);
    }
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        printf("Usage: %s [-kata | -huruf] [word/letter]\n", argv[0]);
        return 1;
    }

    char *option = argv[1];
    char *target = argv[2];

    toLowerCase(target);

    if (strcmp(option, "-kata") == 0)
    {
        char inputFile[MAX_FILENAME];
        strcpy(inputFile, "lirik.txt");

        pid_t pid;
        int pipes[2];
        pipe(pipes);

        pid = fork();

        if (pid < 0)
        {
            fprintf(stderr, "Fork failed.\n");
            return 1;
        }

        if (pid == 0)
        {
            close(pipes[1]);
            char buffer[MAX_BUFFER];
            read(pipes[0], buffer, sizeof(buffer));
            close(pipes[0]);
            childProcessKata(inputFile, target);
        }
        else
        {
            close(pipes[0]);
            parentProcess(inputFile, option);
            write(pipes[1], inputFile, sizeof(inputFile));
            close(pipes[1]);
            wait(NULL);
        }
    }
    else if (strcmp(option, "-huruf") == 0)
    {
        char inputFile[MAX_FILENAME];
        strcpy(inputFile, "lirik.txt");

        pid_t pid;
        int pipes[2];
        pipe(pipes);

        pid = fork();

        if (pid < 0)
        {
            fprintf(stderr, "Fork failed.\n");
            return 1;
        }

        if (pid == 0)
        {
            close(pipes[1]);
            char buffer[MAX_BUFFER];
            read(pipes[0], buffer, sizeof(buffer));
            close(pipes[0]);
            childProcessHuruf(inputFile, target);
        }
        else
        {
            close(pipes[0]);
            parentProcess(inputFile, option);
            write(pipes[1], inputFile, sizeof(inputFile));
            close(pipes[1]);
            wait(NULL);
        }
    }
    else
    {
        printf("Invalid argument. Use -kata or -huruf.\n");
    }

    return 0;
}