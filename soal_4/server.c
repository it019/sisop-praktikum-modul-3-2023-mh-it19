#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h> 
#define PORT 8080

void *clientSide(void *client_socket) {
    int new_socket = *((int *)client_socket);
    char buffer[1024] = {0};
    int valread;

    while (true) {
        memset(buffer, 0, sizeof(buffer));

        valread = read(new_socket, buffer, 1024);
        if (valread <= 0) {
            printf("Client disconnected\n");
            close(new_socket);
            break;
        }

        printf("%s\n", buffer);
    }

    return NULL;
}

int main(int argc, char const *argv[]) {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    int clients[5] = {0};
    int activeClients = 0;

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 5) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    while (true) {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        if (activeClients < 5) {
            clients[activeClients] = new_socket;
            activeClients++;

            pthread_t client_thread;
            if (pthread_create(&client_thread, NULL, clientSide, (void *)&new_socket) != 0) {
                perror("pthread_create");
            }
        } else {
            printf("Server sedang penuh\n");
            close(new_socket);
        }
    }

    return 0;
}
