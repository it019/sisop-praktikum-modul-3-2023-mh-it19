#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
 
#define MAX 1024

// Structure for message queue
struct mesg_buffer {
    long mesg_type;
    char mesg_text[MAX];
} message;

// Function to decrypt Base64 encoded password
char *decryptPassword(const char *encodedPassword) {
    BIO *bio, *b64;
    int length = strlen(encodedPassword);
    char *decodedPassword = (char *)malloc(length);
    memset(decodedPassword, 0, length);

    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_mem_buf(encodedPassword, length);
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
    BIO_read(bio, decodedPassword, length);

    BIO_free_all(bio);

    // Remove newline character if present
    char *newline = strchr(decodedPassword, '\n');
    if (newline != NULL) {
        *newline = '\0';
    }

    return decodedPassword;
}

int main() {
    key_t key;
    int msgid;

    // Generate unique key
key = ftok("sender.c", 'A');
    if (key == -1) {
        perror("ftok");
        exit(EXIT_FAILURE);
    }

    // Create a message queue
    msgid = msgget(key, 0666 | IPC_CREAT);
    if (msgid == -1) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }

    printf("Receiver: Message Queue Created with ID %d\n", msgid);

    // Receive file content through the message queue
    msgrcv(msgid, &message, sizeof(message), 1, 0);

    // Decrypt and display username:password for each line
    char *line = strtok(message.mesg_text, "\n");

    while (line != NULL) {
        char *username = strtok(line, ":");
        char *encodedPassword = strtok(NULL, ":");
        char *password = decryptPassword(encodedPassword);
        printf("Username: %s, Password: %s\n", username, password);
        free(password); // Free dynamically allocated memory
        line = strtok(NULL, "\n");
    }

    // Clean up
    msgctl(msgid, IPC_RMID, NULL);
    printf("Receiver: Message Queue Removed\n");

    return 0;
}
