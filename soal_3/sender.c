#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
  
#define MAX 1024

// Structure for message queue
struct mesg_buffer {
    long mesg_type;
    char mesg_text[MAX];
} message;

int main() {
    key_t key;
    int msgid;

    // Generate unique key
    key = ftok("sender.c", 'A');
    if (key == -1) {
        perror("ftok");
        exit(EXIT_FAILURE);
    }

    // Create a message queue
    msgid = msgget(key, 0666 | IPC_CREAT);
    if (msgid == -1) {
        perror("msgget");
        exit(EXIT_FAILURE);
    }

    printf("Sender: Message Queue Created with ID %d\n", msgid);

    // Read filename from user
    char filename[MAX];
    printf("Masukkan Namafile: ");
    fgets(filename, MAX, stdin);
    // Remove newline character from filename
    filename[strcspn(filename, "\n")] = 0;

    // Open the file for reading
    int file_fd = open(filename, O_RDONLY);
if (file_fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    // Read file content
    ssize_t bytes_read = read(file_fd, message.mesg_text, MAX);
    close(file_fd);

    if (bytes_read == -1) {
        perror("read");
        exit(EXIT_FAILURE);
    }

    // Send the file content through the message queue
    message.mesg_type = 1;
    msgsnd(msgid, &message, bytes_read, 0);

    printf("Sender: File Sent\n");

    // Clean up (sleep for a while to ensure receiver gets a chance to read before deletion)
    sleep(2);
    msgctl(msgid, IPC_RMID, NULL);
    printf("Sender: Message Queue Removed\n");

    return 0;
}
