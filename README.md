# sisop-praktikum-modul-3-2023-MH-IT19

| Nama Anggota | NRP |
| ----------- | ----------- |
| Michael Wayne | 5027221037 |
| Muhammad Harvian Dito Syahputra | 5027221039 |
| Imam Nurhadi | 5027221046 |


## Soal 1

Pada soal nomor 1, praktikan diminta untuk membuat tiga file, yaitu ```belajar.c```, ```yang.c```, dan ```rajin.c```. File belajar.c berisi kode untuk untuk menjalankan perkalian matriks 3x3 dan setiap angkanya dikurang 1. File ```yang.c```dan ```rajin.c``` berisi kode untuk menjalankan transpose matriks dan perhitungan faktorial pada setiap angkanya. Perbedaan dari kedua kode tersebut adalah yang.c menggunakan _thread_ dan _multithread_, sedangkan rajin.c tidak.

Kelompok 19 diharuskan untuk melakukan perhitungan matriks M1[9][2] x M2[2][9] dan menghasilkan matriks R[9][9].

Berikut adalah kode ```belajar.c```

```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROWS1 9
#define COLS1 2
#define ROWS2 2
#define COLS2 9

int main()
{
    int matrix1[ROWS1][COLS1];
    int matrix2[ROWS2][COLS2];
    int result[ROWS1][COLS2];

    srand(time(NULL));

    printf("matriks pertama:\n");
    for (int i = 0; i < ROWS1; i++)
    {
        for (int j = 0; j < COLS1; j++)
        {
            matrix1[i][j] = rand() % 4 + 1;
            printf("%d\t", matrix1[i][j]);
        }
        printf("\n");
    }

    printf("\n");

    printf("matriks kedua:\n");

    for (int i = 0; i < ROWS2; i++)
    {
        for (int j = 0; j < COLS2; j++)
        {
            matrix2[i][j] = rand() % 5 + 1;
            printf("%d\t", matrix2[i][j]);
        }
        printf("\n");
    }

    printf("\n");

    for (int i = 0; i < ROWS1; i++)
    {
        for (int j = 0; j < COLS2; j++)
        {
            result[i][j] = 0;
            for (int k = 0; k < COLS1; k++)
            {
                result[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }

    printf("hasil perkalian sebelum diminus satu:\n");
    for (int i = 0; i < ROWS1; i++)
    {
        for (int j = 0; j < COLS2; j++)
        {
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }

    printf("\n");

    for (int i = 0; i < ROWS1; i++)
    {
        for (int j = 0; j < COLS2; j++)
        {
            result[i][j] = 0;
            for (int k = 0; k < COLS1; k++)
            {
                result[i][j] += matrix1[i][k] * matrix2[k][j];
            }

            result[i][j] -= 1;
        }
    }

    key_t key = ftok("belajar.c", 'R');
    int shmid = shmget(key, sizeof(int) * ROWS1 * COLS2, 0666 | IPC_CREAT);
    int *shared_memory = (int *)shmat(shmid, NULL, 0);
    for (int i = 0; i < ROWS1; i++)
    {
        for (int j = 0; j < COLS2; j++)
        {
            shared_memory[i * COLS2 + j] = result[i][j];
        }
    }

    printf("hasil perkalian setelah diminus satu:\n");
    for (int i = 0; i < ROWS1; i++)
    {
        for (int j = 0; j < COLS2; j++)
        {
            printf("%d\t", result[i][j]);
        }
        printf("\n");
    }

    shmdt(shared_memory);

    return 0;
}
```

Setelah itu, dilakukan transpose matriks dan perhitungan faktorial dalam bentuk _string_ menggunakan konsep _thread_ dan _multithread_. Pada baris terakhir, diberikan sebuah timer untuk menghitung waktu eksekusi pada ```yang.c```

Berikut adalah kode ```yang.c```

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <time.h>

#define ROWS1 9
#define COLS2 9

int *result;
char *transposed[COLS2][ROWS1];

int multiply(int size, int res[], int n)
{
    int carry = 0;
    for (int i = 0; i < size; i++)
    {
        int product = res[i] * n + carry;
        res[i] = product % 10;
        carry = product / 10;
    }

    while (carry)
    {
        res[size] = carry % 10;
        carry = carry / 10;
        size++;
    }

    return size;
}

char *factorialAsString(int n)
{
    int res[1000] = {0};
    int size = 1;
    res[0] = 1;

    for (int i = 2; i <= n; i++)
    {
        size = multiply(size, res, i);
    }

    char *resultStr = (char *)malloc(1001);
    resultStr[0] = '\0';

    for (int i = size - 1; i >= 0; i--)
    {
        char digit[2];
        sprintf(digit, "%d", res[i]);
        strcat(resultStr, digit);
    }

    return resultStr;
}

void *calculateFactorial(void *data)
{
    int *args = (int *)data;
    int row = args[0];
    int col = args[1];
    char *factStr = transposed[col][row];
    char *factResultStr = factorialAsString(atoi(factStr));
    strcpy(transposed[col][row], factResultStr);
    free(factResultStr);
    pthread_exit(NULL);
}

int main()
{
    // Record the start time
    clock_t start_time = clock();

    key_t key = ftok("belajar.c", 'R');
    int shmid = shmget(key, sizeof(int) * ROWS1 * COLS2, 0666);
    result = (int *)shmat(shmid, (void *)0, 0);

    for (int i = 0; i < ROWS1; i++)
    {
        for (int j = 0; j < COLS2; j++)
        {
            transposed[j][i] = (char *)malloc(1001);
            sprintf(transposed[j][i], "%d", result[i * COLS2 + j]);
        }
    }

    pthread_t threads[COLS2][ROWS1];
    for (int i = 0; i < COLS2; i++)
    {
        for (int j = 0; j < ROWS1; j++)
        {
            int *args = (int *)malloc(2 * sizeof(int));
            args[0] = j;
            args[1] = i;
            pthread_create(&threads[i][j], NULL, calculateFactorial, args);
        }
    }

    for (int i = 0; i < COLS2; i++)
    {
        for (int j = 0; j < ROWS1; j++)
        {
            pthread_join(threads[i][j], NULL);
        }
    }

    printf("Hasil transpose dan faktorial matriks:\n");
    for (int i = 0; i < COLS2; i++)
    {
        for (int j = 0; j < ROWS1; j++)
        {
            printf("%s\t", transposed[i][j]);
        }
        printf("\n");
    }

    shmdt(result);

    for (int i = 0; i < COLS2; i++)
    {
        for (int j = 0; j < ROWS1; j++)
        {
            free(transposed[i][j]);
        }
    }

    // Record the end time
    clock_t end_time = clock();

    // Calculate the total elapsed time
    double elapsed_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;

    printf("Total elapsed time: %f seconds\n", elapsed_time);

    return 0;
}
```

Pada kode ```rajin.c```, dilakukan peritungan yang sama dengan yang.c. Namun, ```rajin.c``` tidak menggunakan konsep _thread_ dan _multithread_. Pada baris terakhir, diberikan sebuah timer untuk menghitung waktu eksekusi pada ```rajin.c```

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define ROWS1 9
#define COLS2 9

int *result;
char *transposed[COLS2][ROWS1];

int multiply(int size, int res[], int n)
{
    int carry = 0;
    for (int i = 0; i < size; i++)
    {
        int product = res[i] * n + carry;
        res[i] = product % 10;
        carry = product / 10;
    }

    while (carry)
    {
        res[size] = carry % 10;
        carry = carry / 10;
        size++;
    }

    return size;
}

char *factorialAsString(int n)
{
    int res[1000] = {0};
    int size = 1;
    res[0] = 1;

    for (int i = 2; i <= n; i++)
    {
        size = multiply(size, res, i);
    }

    char *resultStr = (char *)malloc(1001);
    resultStr[0] = '\0';

    for (int i = size - 1; i >= 0; i--)
    {
        char digit[2];
        sprintf(digit, "%d", res[i]);
        strcat(resultStr, digit);
    }

    return resultStr;
}

int main()
{
    clock_t start_time = clock();

    key_t key = ftok("belajar.c", 'R');
    int shmid = shmget(key, sizeof(int) * ROWS1 * COLS2, 0666);
    result = (int *)shmat(shmid, (void *)0, 0);

    for (int i = 0; i < ROWS1; i++)
    {
        for (int j = 0; j < COLS2; j++)
        {
            transposed[j][i] = (char *)malloc(1001);
            sprintf(transposed[j][i], "%d", result[i * COLS2 + j]);
            char *factResultStr = factorialAsString(atoi(transposed[j][i]));
            strcpy(transposed[j][i], factResultStr);
            free(factResultStr);
        }
    }

    printf("Hasil transpose dan faktorial matriks:\n");
    for (int i = 0; i < COLS2; i++)
    {
        for (int j = 0; j < ROWS1; j++)
        {
            printf("%s\t", transposed[i][j]);
        }
        printf("\n");
    }

    shmdt(result);

    for (int i = 0; i < COLS2; i++)
    {
        for (int j = 0; j < ROWS1; j++)
        {
            free(transposed[i][j]);
        }
    }

    // Record the end time
    clock_t end_time = clock();

    // Calculate the total elapsed time
    double elapsed_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;

    printf("Total elapsed time: %f seconds\n", elapsed_time);

    return 0;
}
```

Setelah semua proses telah dijalankan, didapat sebuah data bahwa waktu eksekusi ```rajin.c``` lebih cepat dibandingkan dengan ```yang.c```.


## Soal 2

Pada soal nomor 2, praktikan diminta untuk membuat program semacam ctrl + F menggunakan konsep _pipes_, _fork_, _parent process_, dan _child process_. Praktikan diminta untuk mencari banyaknya huruf atau kata dari file ```lirik.txt``` dan disimpan ke dalam file ```frekuensi.log``` (perhitungan jumlah frekuensi kemunculan kata atau huruf menggunakan _case-sensitive search_).

Berikut adalah function pertama untuk menjalan _parent_ dan _child process_. Kedua proses tersebut akan dihubungkan menggunakan konsep _pipes_.

```c
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        printf("Usage: %s [-kata | -huruf] [word/letter]\n", argv[0]);
        return 1;
    }

    char *option = argv[1];
    char *target = argv[2];

    // toLowerCase(target);

    if (strcmp(option, "-kata") == 0)
    {
        char inputFile[MAX_FILENAME];
        strcpy(inputFile, "lirik.txt");

        pid_t pid;
        int pipes[2];
        pipe(pipes);

        pid = fork();

        if (pid < 0)
        {
            fprintf(stderr, "Fork failed.\n");
            return 1;
        }

        if (pid == 0)
        {
            close(pipes[1]);
            char buffer[MAX_BUFFER];
            read(pipes[0], buffer, sizeof(buffer));
            close(pipes[0]);
            childProcessKata(inputFile, target);
        }
        else
        {
            close(pipes[0]);
            parentProcess(inputFile, option);
            write(pipes[1], inputFile, sizeof(inputFile));
            close(pipes[1]);
            wait(NULL);
        }
    }
    else if (strcmp(option, "-huruf") == 0)
    {
        char inputFile[MAX_FILENAME];
        strcpy(inputFile, "lirik.txt");

        pid_t pid;
        int pipes[2];
        pipe(pipes);

        pid = fork();

        if (pid < 0)
        {
            fprintf(stderr, "Fork failed.\n");
            return 1;
        }

        if (pid == 0)
        {
            close(pipes[1]);
            char buffer[MAX_BUFFER];
            read(pipes[0], buffer, sizeof(buffer));
            close(pipes[0]);
            childProcessHuruf(inputFile, target);
        }
        else
        {
            close(pipes[0]);
            parentProcess(inputFile, option);
            write(pipes[1], inputFile, sizeof(inputFile));
            close(pipes[1]);
            wait(NULL);
        }
    }
    else
    {
        printf("Invalid argument. Use -kata or -huruf.\n");
    }

    return 0;
}
```

Setelah itu, _parent_ dan _child process_ akan dipanggil ke atas untuk menjalankan proses pencarian kata sehingga kode keseluruhannya menjadi seperti berikut

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

#define MAX_FILENAME 256
#define MAX_BUFFER 1024

void parentProcess(char *inputFile, char *option)
{
    FILE *file = fopen(inputFile, "r");
    FILE *output = fopen("thebeatles.txt", "w");

    if (file == NULL)
    {
        printf("Failed to open the input file.\n");
        return;
    }

    if (output == NULL)
    {
        printf("Failed to create the output file.\n");
        return;
    }

    char c;
    while ((c = fgetc(file)) != EOF)
    {
        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == ' ' || c == '\n')
        {
            fputc(c, output);
        }
    }

    fclose(file);
    fclose(output);
}

void childProcessKata(char *inputFile, char *target)
{
    FILE *file = fopen(inputFile, "r");

    if (file == NULL)
    {
        printf("Failed to open the input file.\n");
        return;
    }

    char word[100];
    int count = 0;

    while (fscanf(file, "%99[^a-zA-Z]%99[a-zA-Z]", word, word) != EOF)
    {
        if (strcmp(word, target) == 0)
        {
            count++;
        }
    }

    fclose(file);

    printf("Kata '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", target, count);
    FILE *logFile = fopen("frekuensi.log", "a");
    if (logFile != NULL)
    {
        time_t now;
        struct tm *tm_info;
        time(&now);
        tm_info = localtime(&now);
        char time_str[20];
        strftime(time_str, 20, "%d/%m/%y %H:%M:%S", tm_info);

        fprintf(logFile, "[%s] [KATA] Kata '%s' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", time_str, target, count);
        fclose(logFile);
    }
}

void childProcessHuruf(char *inputFile, char *target)
{
    FILE *file = fopen(inputFile, "r");

    if (file == NULL)
    {
        printf("Failed to open the input file.\n");
        return;
    }

    char c;
    int count = 0;

    while ((c = fgetc(file)) != EOF)
    {
        if (c == target[0])
        {
            count++;
        }
    }

    fclose(file);

    printf("Huruf '%c' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", target[0], count);
    FILE *logFile = fopen("frekuensi.log", "a");
    if (logFile != NULL)
    {
        time_t now;
        struct tm *tm_info;
        time(&now);
        tm_info = localtime(&now);
        char time_str[20];
        strftime(time_str, 20, "%d/%m/%y %H:%M:%S", tm_info);

        fprintf(logFile, "[%s] [HURUF] Huruf '%c' muncul sebanyak %d kali dalam file 'thebeatles.txt'\n", time_str, target[0], count);
        fclose(logFile);
    }
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        printf("Usage: %s [-kata | -huruf] [word/letter]\n", argv[0]);
        return 1;
    }

    char *option = argv[1];
    char *target = argv[2];

    if (strcmp(option, "-kata") == 0)
    {
        char inputFile[MAX_FILENAME];
        strcpy(inputFile, "lirik.txt");

        pid_t pid;
        int pipes[2];
        pipe(pipes);

        pid = fork();

        if (pid < 0)
        {
            fprintf(stderr, "Fork failed.\n");
            return 1;
        }

        if (pid == 0)
        {
            close(pipes[1]);
            char buffer[MAX_BUFFER];
            read(pipes[0], buffer, sizeof(buffer));
            close(pipes[0]);
            childProcessKata(inputFile, target);
        }
        else
        {
            close(pipes[0]);
            parentProcess(inputFile, option);
            write(pipes[1], inputFile, sizeof(inputFile));
            close(pipes[1]);
            wait(NULL);
        }
    }
    else if (strcmp(option, "-huruf") == 0)
    {
        char inputFile[MAX_FILENAME];
        strcpy(inputFile, "lirik.txt");

        pid_t pid;
        int pipes[2];
        pipe(pipes);

        pid = fork();

        if (pid < 0)
        {
            fprintf(stderr, "Fork failed.\n");
            return 1;
        }

        if (pid == 0)
        {
            close(pipes[1]);
            char buffer[MAX_BUFFER];
            read(pipes[0], buffer, sizeof(buffer));
            close(pipes[0]);
            childProcessHuruf(inputFile, target);
        }
        else
        {
            close(pipes[0]);
            parentProcess(inputFile, option);
            write(pipes[1], inputFile, sizeof(inputFile));
            close(pipes[1]);
            wait(NULL);
        }
    }
    else
    {
        printf("Invalid argument. Use -kata or -huruf.\n");
    }

    return 0;
}
```


## Soal 3

Jadi pada soal nomer 3, terdapat beberapa case yaitu melakukan decrypt content file, melakukan autentikasi berdasarkan username dan password yang sudah didecyrpt berdasarkan file users, lalu melakukan TRANSFER berdasarkan file name dengan melihatkan satus ukuran KB file. Dan memberikan case UNKNOWN COMMAND jika receiver tidak dapat menghandle beberapa perintah sender.


### sender.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define MAX_SIZE 100

struct msg_buffer {
    long msg_type;
    char msg_text[MAX_SIZE];
};

int main(int argc, char *argv[]) {
    key_t key;
    int msgid;
    struct msg_buffer message;
    int file_descriptor;

    if (argc < 2) {
        printf("Usage: %s <CREDS or AUTH:username password or TRANSFER filename>\n", argv[0]);
        return 1;
    }

    // Membuat key unik
    key = ftok("sender.c", 65);

    // Membuat message queue
    msgid = msgget(1, 0666 | IPC_CREAT);
    message.msg_type = 1;

    // Menangani perintah CREDS
    if (strcmp(argv[1], "CREDS") == 0) {
        strcpy(message.msg_text, "CREDS");
    }
    
    if (strcmp(argv[1], "Makan") == 0) {
        strcpy(message.msg_text, "Makan");
    }
    
    // Menangani perintah AUTH
    else if (strstr(argv[1], "AUTH:") != NULL) {
        strcpy(message.msg_text, argv[1]);
    }
    // Menangani perintah TRANSFER
    else if (strcmp(argv[1], "TRANSFER") == 0 && argc == 3) {
        char filepath[MAX_SIZE];
        snprintf(filepath, sizeof(filepath), "Sender/%s", argv[2]);

        // Baca ukuran file
        struct stat st;
        if (stat(filepath, &st) == -1) {
            perror("Error: Cannot get file size");
            return 1;
        }

        // Menghitung ukuran file dalam kilobit
        long file_size_kb = (st.st_size + 1023) / 1024;  // Pembulatan ke atas

        // Mengirim nama file ke receiver
        snprintf(message.msg_text, sizeof(message.msg_text), "TRANSFER %s", argv[2]);
        msgsnd(msgid, &message, sizeof(message), 0);

        // Membuka file untuk dibaca dan mengirimkan isinya
        file_descriptor = open(filepath, O_RDONLY);
        if (file_descriptor == -1) {
            perror("Error: Cannot open file");
            return 1;
        }

        // Membaca dan mengirimkan file content ke message queue
        ssize_t bytesRead;
        while ((bytesRead = read(file_descriptor, message.msg_text, sizeof(message.msg_text))) > 0) {
            msgsnd(msgid, &message, bytesRead, 0);
        }

        close(file_descriptor);

        // Mengirim status ukuran file ke receiver
        snprintf(message.msg_text, sizeof(message.msg_text), "%ld KB", file_size_kb);
        msgsnd(msgid, &message, sizeof(message), 0);
    }
    else {
        printf("Invalid command.\n");
        return 1;
    }

    // Mengirim pesan ke receiver
    msgsnd(msgid, &message, sizeof(message), 0);

    printf("Command sent to receiver: %s\n", message.msg_text);
    return 0;
}
```

### receiver.c

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MAX_SIZE 200

struct User {
    char username[MAX_SIZE];
    char password[MAX_SIZE];
};

struct msg_buffer {
    long msg_type;
    char msg_text[MAX_SIZE];
    long file_size;
    char file_content[MAX_SIZE];
};

struct User users[] = {
    {"Mayuri", "TuTuRuuu"},
    {"Onodera", "K0sak!"},
    {"Johan", "L!3b3rt"},
    {"Seki", "Yuk!n3"},
    {"Ayanokouji", "K!yot4kA"},
    {"Ishigami", "Ber0t4kSenku"},
    {"Satoru", "Su9uru"},
    {"Dazai", "0s4mu"},
    {"Rudeus", "Gr3yR4t"},
    {"Ichigo", "Kur0s4k!"}
};
void base64Decode(const char *input, char *output) {
    BIO *bio, *b64;
    int decodedSize = 0;
    
    // Menghitung panjang output yang didekripsi
    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    bio = BIO_new_mem_buf(input, -1);
    bio = BIO_push(b64, bio);
    decodedSize = BIO_read(bio, output, strlen(input));
    output[decodedSize] = '\0';
    
    BIO_free_all(bio);
}

bool authenticate(char *authUsername, char *authPassword) {
    for (int i = 0; i < sizeof(users) / sizeof(users[0]); ++i) {
        if (strcmp(authUsername, users[i].username) == 0 &&
            strcmp(authPassword, users[i].password) == 0) {
            return true; // Autentikasi berhasil
        }
    }
    return false; // Autentikasi gagal
}

int main() {
    key_t key;
    int msgid;
    struct msg_buffer message;
    
    // Membuat key unik
    key = ftok("receiver.c", 65);
    
    // Mengambil message queue
    msgid = msgget(1, 0666 | IPC_CREAT);
    
    printf("Receiver: Message Queue Created with ID %d\n", msgid);
    
    // Menerima pesan dari sender
    msgrcv(msgid, &message, sizeof(message), 1, 0);
    printf("Received message: %s\n", message.msg_text);
    
    // Memeriksa perintah dari sender
    // Memeriksa perintah dari sender
    if (strcmp(message.msg_text, "CREDS") == 0) {
        // Membaca file users dan menyimpannya ke dalam string
        FILE *file = fopen("users", "r");
        if (file == NULL) {
            fprintf(stderr, "Error: Cannot open file users.\n");
            exit(EXIT_FAILURE);	
        }
        
        char line[MAX_SIZE];
        // Membaca setiap baris dari file users
        while (fgets(line, sizeof(line), file) != NULL) {
            char *username = strtok(line, ":");
            char *encodedPassword = strtok(NULL, ":\n");
            
            char decryptedPassword[MAX_SIZE];
            // Dekripsi password yang dienkripsi dalam base64
            base64Decode(encodedPassword, decryptedPassword);
            
            // Menampilkan username dan password yang telah didekripsi
            printf("Username: %s, Password: %s\n", username, decryptedPassword);
        }
        
        fclose(file);
    }  else if (strstr(message.msg_text, "AUTH:") != NULL) {
        printf("Received AUTH command.\n");
        
        char *authCommand = strtok(message.msg_text, ":");
        char *authUsername = strtok(NULL, " ");
        char *authPassword = strtok(NULL, " ");
        
        // Memanggil fungsi autentikasi dan menampilkan hasil
        if (authenticate(authUsername, authPassword)) {
            printf("Authentication successful for user: %s\n", authUsername);
        } else {
            printf("Authentication failed for user: %s\n", authUsername);
        }
   }
   
   
    else if (strstr(message.msg_text, "TRANSFER") != NULL) {
        char filename[MAX_SIZE];
        // Menerima nama file
        msgrcv(msgid, &message, sizeof(message), 1, 0);
        strcpy(filename, message.msg_text);
        printf("%s\n", filename);

        // Menerima file content
        msgrcv(msgid, &message, sizeof(message), 1, 0);
        printf("File Content:\n%s\n", message.msg_text);

        // Menerima status ukuran file
        msgrcv(msgid, &message, sizeof(message), 1, 0);
        printf("%s\n", message.msg_text);
    }
    
    
    else {
        printf("UNKNOWN COMMAND\n");
    }
    
    // Menghapus message queue
    msgctl(msgid, IPC_RMID, NULL);
    
    return 0;
}
```

**a.** 2 Program dibuat agar hanya dapat mengirim perintah berupa string

```c
struct msg_buffer {
    long msg_type;
    char msg_text[MAX_SIZE];
};

int main(int argc, char *argv[]) {
    key_t key;
    int msgid;
    struct msg_buffer message;
    int file_descriptor;

    if (argc < 2) {
        printf("Usage: %s <CREDS or AUTH:username password or TRANSFER filename>\n", argv[0]);
        return 1;
    }

    // Membuat key unik
    key = ftok("sender.c", 65);

    // Membuat message queue
    msgid = msgget(1, 0666 | IPC_CREAT);
    message.msg_type = 1;

    // Menangani perintah CREDS
    if (strcmp(argv[1], "CREDS") == 0) {
        strcpy(message.msg_text, "CREDS");
    }
    
    if (strcmp(argv[1], "Makan") == 0) {
        strcpy(message.msg_text, "Makan");
    }
    
    // Menangani perintah AUTH
    else if (strstr(argv[1], "AUTH:") != NULL) {
        strcpy(message.msg_text, argv[1]);
    }
    // Menangani perintah TRANSFER
    else if (strcmp(argv[1], "TRANSFER") == 0 && argc == 3) {

        // Membaca dan mengirimkan file content ke message queue
        ssize_t bytesRead;
        while ((bytesRead = read(file_descriptor, message.msg_text, sizeof(message.msg_text))) > 0) {
            msgsnd(msgid, &message, bytesRead, 0);
        }

        close(file_descriptor);

        // Mengirim status ukuran file ke receiver
        snprintf(message.msg_text, sizeof(message.msg_text), "%ld KB", file_size_kb);
        msgsnd(msgid, &message, sizeof(message), 0);
    }
    else {
        printf("Invalid command.\n");
        return 1;
    }
    
    // Mengirim pesan ke receiver
    msgsnd(msgid, &message, sizeof(message), 0);

    printf("Command sent to receiver: %s\n", message.msg_text);
    return 0;
}
```
pada program sender. ini, sender hanya dapat mengirim STRING yang akan dijadikan pengkondisian receiver untuk mengeksekusi beberapa fungsi kedepannya. Penggunaan message.msg_text untuk variabel text yang akan dikirim.

**b.** Menggunakan base64 dan mengirim perintah CREDS agar receiver melakuka decryot oada fuke users lalu ditampilkan

sender.c
 ```c
if (strcmp(argv[1], "CREDS") == 0) {
        strcpy(message.msg_text, "CREDS");
    }
```
receiver.c
untuk fungsi decrypt
```c
void base64Decode(const char *input, char *output) {
    BIO *bio, *b64;
    int decodedSize = 0;
    
    // Menghitung panjang output yang didekripsi
    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    bio = BIO_new_mem_buf(input, -1);
    bio = BIO_push(b64, bio);
    decodedSize = BIO_read(bio, output, strlen(input));
    output[decodedSize] = '\0';
    
    BIO_free_all(bio);
}
```

Program receiver untuk menerima perintah sender dan melakukan decrypt dengan memisahkan username dan password untuk di decode, lalu ditampilkan.
 ```c
if (strcmp(message.msg_text, "CREDS") == 0) {
        // Membaca file users dan menyimpannya ke dalam string
        FILE *file = fopen("users", "r");
        if (file == NULL) {
            fprintf(stderr, "Error: Cannot open file users.\n");
            exit(EXIT_FAILURE);	
        }
        
        char line[MAX_SIZE];
        // Membaca setiap baris dari file users
        while (fgets(line, sizeof(line), file) != NULL) {
            char *username = strtok(line, ":");
            char *encodedPassword = strtok(NULL, ":\n");
            
            char decryptedPassword[MAX_SIZE];
            // Dekripsi password yang dienkripsi dalam base64
            base64Decode(encodedPassword, decryptedPassword);
            
            // Menampilkan username dan password yang telah didekripsi
            printf("Username: %s, Password: %s\n", username, decryptedPassword);
        }
        
        fclose(file);
    }  
```
dokumentasi demo

**c.** Sender memberikan perintah AUTH: username password, receiver akan menerima perintah dan melakukan autentikasi apakah username dan password yang diberikan cocok dengan file username yang didecryot sebelumnya 

sender.c
```c
// Menangani perintah AUTH
    else if (strstr(argv[1], "AUTH:") != NULL) {
        strcpy(message.msg_text, argv[1]);
    }
```
receiver.c
mendefinisikan users
```c
struct User {
    char username[MAX_SIZE];
    char password[MAX_SIZE];
};


struct User users[] = {
    {"Mayuri", "TuTuRuuu"},
    {"Onodera", "K0sak!"},
    {"Johan", "L!3b3rt"},
    {"Seki", "Yuk!n3"},
    {"Ayanokouji", "K!yot4kA"},
    {"Ishigami", "Ber0t4kSenku"},
    {"Satoru", "Su9uru"},
    {"Dazai", "0s4mu"},
    {"Rudeus", "Gr3yR4t"},
    {"Ichigo", "Kur0s4k!"}
};
```
fungsi untuk melakukan pengecekan
```c
bool authenticate(char *authUsername, char *authPassword) {
    for (int i = 0; i < sizeof(users) / sizeof(users[0]); ++i) {
        if (strcmp(authUsername, users[i].username) == 0 &&
            strcmp(authPassword, users[i].password) == 0) {
            return true; // Autentikasi berhasil
        }
    }
    return false; // Autentikasi gagal
}
```
int main untuk menampilkan
```c
else if (strstr(message.msg_text, "AUTH:") != NULL) {
        printf("Received AUTH command.\n");
        
        char *authCommand = strtok(message.msg_text, ":");
        char *authUsername = strtok(NULL, " ");
        char *authPassword = strtok(NULL, " ");
        
        // Memanggil fungsi autentikasi dan menampilkan hasil
        if (authenticate(authUsername, authPassword)) {
            printf("Authentication successful for user: %s\n", authUsername);
        } else {
            printf("Authentication failed for user: %s\n", authUsername);
        }
   }
```

Program receiver akan menyocokkan username dan password pada struct username yang didapatkann pada hasil perintah CREDS, dan fungsi authenticate akan menyaring input sender apakah itu sesuai dengan yang didapatkan fungsi decrypt, jika iya maka akan berhasil dan jika tidak maka akan menampilkan Authentication failed. Hasil input sender akan diterima menjadi authUsername dan authPassword.


**d&e** memberikan perintah TRANSFER filename dengan sender mengirim file dari folder Sender ke receiver, dan receiver akan menyimpan hasil file ke folder receiver dan akan menamilkannya. Program juga akan menampilkan status ukuran pada setiap pengiriman file yang berhasil


sender.c untuk memberikan perintah TRANSFER
 ```c
else if (strcmp(argv[1], "TRANSFER") == 0 && argc == 3) {
        char filepath[MAX_SIZE];
        snprintf(filepath, sizeof(filepath), "Sender/%s", argv[2]);

        // Baca ukuran file
        struct stat st;
        if (stat(filepath, &st) == -1) {
            perror("Error: Cannot get file size");
            return 1;
        }

        // Menghitung ukuran file dalam kilobit
        long file_size_kb = (st.st_size + 1023) / 1024;  // Pembulatan ke atas

        // Mengirim nama file ke receiver
        snprintf(message.msg_text, sizeof(message.msg_text), "TRANSFER %s", argv[2]);
        msgsnd(msgid, &message, sizeof(message), 0);

        // Membuka file untuk dibaca dan mengirimkan isinya
        file_descriptor = open(filepath, O_RDONLY);
        if (file_descriptor == -1) {
            perror("Error: Cannot open file");
            return 1;
        }

        // Membaca dan mengirimkan file content ke message queue
        ssize_t bytesRead;
        while ((bytesRead = read(file_descriptor, message.msg_text, sizeof(message.msg_text))) > 0) {
            msgsnd(msgid, &message, bytesRead, 0);
        }

        close(file_descriptor);

        // Mengirim status ukuran file ke receiver
        snprintf(message.msg_text, sizeof(message.msg_text), "%ld KB", file_size_kb);
        msgsnd(msgid, &message, sizeof(message), 0);
    }
```

receiver.c untuk menerima dan menampilkan fungsi TRANSFER

 ```c
else if (strstr(message.msg_text, "TRANSFER") != NULL) {
        char filename[MAX_SIZE];
        // Menerima nama file
        msgrcv(msgid, &message, sizeof(message), 1, 0);
        strcpy(filename, message.msg_text);
        printf("%s\n", filename);

        // Menerima file content
        msgrcv(msgid, &message, sizeof(message), 1, 0);
        printf("File Content:\n%s\n", message.msg_text);

        // Menerima status ukuran file
        msgrcv(msgid, &message, sizeof(message), 1, 0);
        printf("%s\n", message.msg_text);
    }
```
3f. pada program 3f akan menunjukkan UNKNOWN COMMAND jika receiver tidak dapat menerima perintah pada sender.c 

```c
else {
        printf("UNKNOWN COMMAND\n");
    }
```


```c
 if (strcmp(argv[1], "Makan") == 0) {
        strcpy(message.msg_text, "Makan");
    }
```

program receiver tidak dapat menerima string Makan pada perintah sender, sehingga receiver memberikan percabangan terakhir yaitu pesan error UNKNOWN COMMAND

Dokumentasi demo soal nomor 3

b. 
![credssender](https://drive.google.com/uc?id=1bCYmYxpLcjMb3iWsnZmZaIbv3pDamuFl)
![credsreceiver](https://drive.google.com/uc?id=1UB9jG9LXNRAWLdsr96HMSHr1LbEa4gZk)

c.
![authsender](https://drive.google.com/uc?id=1IfxvwEsngcBB14NiSeP6BppUumsFYZKR) 
![authreceiver](https://drive.google.com/uc?id=1F50idqdLavXeuJNg81nhks34FAHzUfZ-) 

d dan e. 
![transfersender](https://drive.google.com/uc?id=1xJxbzl00sXHyHLmdZ9qc_FVAwSr0V5P4) 
![transfersender](https://drive.google.com/uc?id=1SQQ3X607YOafA2acSLoosOyJ4YZjF7Bd)

f. 
![errorsender](https://drive.google.com/uc?id=19_QB5-GJ4brFv1FmW_3uB2l0-5EbTUGE) 
![errorsender](https://drive.google.com/uc?id=150iX0mS9MeZ1oExSzHMSrmz4cdc6X4zq) 




## Soal 4
### server.c
```c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h> 
#define PORT 8080

void *clientSide(void *client_socket) {
    int new_socket = *((int *)client_socket);
    char buffer[1024] = {0};
    int valread;

    while (true) {
        memset(buffer, 0, sizeof(buffer));

        valread = read(new_socket, buffer, 1024);
        if (valread <= 0) {
            printf("Client disconnected\n");
            close(new_socket);
            break;
        }

        printf("%s\n", buffer);
    }

    return NULL;
}

int main(int argc, char const *argv[]) {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    int clients[5] = {0};
    int activeClients = 0;

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 5) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    while (true) {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        if (activeClients < 5) {
            clients[activeClients] = new_socket;
            activeClients++;

            pthread_t client_thread;
            if (pthread_create(&client_thread, NULL, clientSide, (void *)&new_socket) != 0) {
                perror("pthread_create");
            }
        } else {
            printf("Server sedang penuh\n");
            close(new_socket);
        }
    }

    return 0;
}
```

Pada soal nomor 4, server diharuskan untuk menerima pesan dan menampilkan pesan saja. Server dibuat menggunakan socket yang terhubung ke maksimal 5 client. Berikut adalah penjelasan program server.c yang telah dibuat:

- `#include <stdio.h>` adalah preprocessor yang memasukkan standar operasi input dan output
- `#include <sys/socket.h>` adalah preprocessor yang menyimpan header fungsi-fungsi pemrograman socket
- `#include <stdlib.h>` berfungsi sebagai file header standar untuk fungsi-fungsi umum
- `#include <stdbool.h>` adalah preprocessor yang memasukkan header tipe data boolean
- `#include <netinet/in.h>` adalah preprocessor yang mendefinisikan struktur data dan konstanta yang digunakan dalam jaringan
- `#include <string.h>` adalah preprocessor yang bertugas untuk fungsi-fungsi string
- `#include <unistd.h>` adalah preprocessor yang memasukkan file header untuk fungsi-fungsi sistem
- `#include <pthread.h>` adalah preprocessor yang memasukkan header untuk multithreading
- `#define PORT 8080` mendefinisikan variabel PORT secara global dengan nilai 8080 sebagai PORT yang akan digunakan untuk menghubungkan server dan client

Kemudian pada fungsi `int main(int argc, char const *argv[]) {...}`, dibuat socket sebagai server. Berikut adalah penjelasan dari fungsi main:

- `int server_fd, new_socket` mendefinisikan variabel `server_fd` yang digunakan sebagai deskriptor socket server dan `new_socket` sebagai deskriptor socket client
- `struct sockaddr_in address` mendefinsikan sockaddr_in sebagai struktur yang menyimpan informasi alamat server
- `int opt = 1` mendefinisikan variabel opt sebagai 1 yang digunakan untuk opsi socket
- `int addrlen = sizeof(address)` mendefinisikan addrlen sebagai ukuran dari `address`
- `int clients[5] = {0}` mendefinisikan array client dengan ukuran 5 sebagai deskriptor jumlah client yang berjalan
- `int activeClients = 0` mendefinisikan activeClients dengan nilai 0 yang digunakan untuk mengetahui jumlah client yang berjalan
- `if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }` 
adalah percabangan if yang digunakan untuk mencoba membuat server socket. Jika gagal maka akan mencetak "socket failed"
- `if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }`
adalah percabangan if yang digunakan untuk mengatur opsi socket agar dapat menggunakan kembali alamat dan port yang digunakan. Jika gagal, maka akan mencetak pesan kesalahan
- `address.sin_family = AF_INET` mengatur family protokol alamt ke IPv4
- `address.sin_addr.s_addr = INADDR_ANY` mengatur alamat IP server agar dapat menerima koneksi jaringan
- `address.sin_port = htons(PORT)` mengatur port server dengan variabel PORT yang bernilai 8080 sebelumnya
- `if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }` 
adalah percabangan if yang mencoba mengikat server socket ke alamat dan port yang telah ditentukan. Jika gagal maka akan mencetak "bind failed"
- `if (listen(server_fd, 5) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }` 
adalah percabangan if yang mengatur server dalam mode listen agar dapat menerima koneksi dari client. Parameter 5 digunakan agar server dapat menerima hingga 5 koneksi. Jika gagal, maka akan mengeluarkan pesan error

Pada fungsi main ini terdapat sebuah loop yaitu `while (true) {...}` untuk menjalankan server secara terus menerus. Penjelasannya adalah sebagai berikut:

- `if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }` 
adalah percabangan if yang digunakan untuk menerima koneksi dari client dan menyimpannya dalam new_socket. Jika gagal, maka akan mencetak pesan kesalahan
- `if (activeClients < 5) {...}` adalah percabangan untuk menerima activeClients hingga sampai 5 client saja.
- `clients[activeClients] = new_socket` menedefinisikan socket client baru yang disimpan dalam array clients dan activeClients akan diperbaharui jika masih ada kapasitas
- `activeClients++` increment nilai activeClients jika ada client yang masuk
- `pthread_t client_thread` mendefinisikan variabel cleint_thread untuk menangani thread client
- `if (pthread_create(&client_thread, NULL, clientSide, (void *)&new_socket) != 0) {
                perror("pthread_create");
            }` 
adalah percabangan if yang digunakan untuk membuat thread baru dengan menjalankan fungsi `clientSide` untuk menangani koneksi dengan Client. Jika gagal, maka akan mencetak pesan error
- `else {
            printf("Server sedang penuh\n");
            close(new_socket);
        }`
adalah percabangan else dari blok `if (activeClients < 5) {...}`. Jika server sudah berisi 5 client, maka akan mencetak pesan "server sedang penuh" dan new_socket akan ditutup

Kemudian terdapat fungsi `void *clientSide(void *client_socket) {...}` yang berfungsi untuk menangani client. Penjelasan fungsinya adalah sebagai berikut:

- `int new_socket = *((int *)client_socket)` melakukan konversi dari pointer client_socket ke variabel new_socket
- `char buffer[1024] = {0}` mendefinisikan array character buffer dengan ukuran 1024 yang digunakan untuk membaca data client
- `int valread` mendefinisikan variabel valread yang digunakan untuk menyimpan nilai dari fungsi read()

Terdapat sebuah loop `while (true) {...}` pada fungsi ini yang berfungsi untuk menerima client dan pesannya secara terus menerus. Penjelasan loopnya adalah sebagai berikut:

- `memset(buffer, 0, sizeof(buffer))` digunakan untuk mengosongkan buffer agar pesan tidak bertabrakan
- `valread = read(new_socket, buffer, 1024)` digunakan untuk membaca data dari client yang terhubung ke new_socket dan menyimpannya ke valread
- `if (valread <= 0) {
            printf("Client disconnected\n");
            close(new_socket);
            break;
        }`
adalah percabangan if yang berfungsi untuk mendeteksi apakah client sudah keluar dari server. Jika valread bernilai 0 atau kurang dari 0, maka client memutus koneksi. Jika bernilai true, maka akan mencetak pesan "client_disconnected", kemudian akan menutup socket client dan akan menghentikan loop dengan break.
- `printf("%s\n", buffer)` berfungsi untuk mencetak pesan yang dikirim oleh client
- `return NULL` berfungsi untuk mengakhiri eksekusi fungsi

### client.c
```c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080

int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char message[1024] = {0};
    char fullMessage[2048] = {0};

    char username[50];
    printf("Enter your username: ");
    fgets(username, sizeof(username), stdin);
    username[strlen(username) - 1] = '\0'; 

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    while (true) {
        printf("Enter your message: ");
        fgets(message, sizeof(message), stdin);
        message[strlen(message) - 1] = '\0';

        snprintf(fullMessage, sizeof(fullMessage), "%s : %s", username, message);

        send(sock, fullMessage, strlen(fullMessage), 0);
    }

    return 0;
}
```

Pada soal nomor 4, client diharuskan untuk dapat mengirim pesan ke server. Client dihubungkan dengan server melalui socket. Berikut adalah penjelasan program client:

- `#include <stdio.h>` adalah preprocessor yang memasukkan standar operasi input dan output
- `#include <sys/socket.h>` adalah preprocessor yang menyimpan header fungsi-fungsi pemrograman socket
- `#include <stdlib.h>` berfungsi sebagai file header standar untuk fungsi-fungsi umum
- `#include <stdbool.h>` adalah preprocessor yang memasukkan header tipe data boolean
- `#include <netinet/in.h>` adalah preprocessor yang mendefinisikan struktur data dan konstanta yang digunakan dalam jaringan
- `#include <string.h>` adalah preprocessor yang bertugas untuk fungsi-fungsi string
- `#include <unistd.h>` adalah preprocessor yang memasukkan file header untuk fungsi-fungsi sistem
- `#include <arpa/inet.h>` adalah preprocessor yang memasukkan file header untuk fungsi-fungsi yang berhubungan dengan alamat internet
- `#define PORT 8080` mendefinisikan variabel PORT secara global dengan nilai 8080 sebagai PORT yang akan digunakan untuk menghubungkan server dan client

Kemudian terdapat fungsi `int main(int argc, char const *argv[]) {..}` yang berfungsi sebagai connecting client ke server dan mengirim pesan ke server. Berikut adalah penjelasan dari fungsi main:

- `struct sockaddr_in address` mendefinsikan sockaddr_in sebagai struktur yang menyimpan informasi alamat client
- `int sock = 0, valread` mendefinisikan variabel sock sebagai deskriptor socket dan variabel valread untuk menyimpan jumlah byte yang berhasil dibaca
- `struct sockaddr_in serv_addr` mendefisiniskan sockaddr_in sebagai struktur yang digunakan untuk menyimpan informasi alamat server
- `char message[1024] = {0}` mendefinisikan array character variabel message dengan ukuran 1024 sebagai pesan dari client
- `char fullMessage[2048] = {0}` mendefinisikan array character fullMessage dengan ukuran 2048 sebagai pesan lengkap yang akan dikirim ke server yang akan berisi username client dan pesannya
- `char username[50]` mendefinisikan array character username berukuran 50 yang akan berisi username client
- `printf("Enter your username: ")` mencetak pesan ke layar client agar memasukkan username
- `fgets(username, sizeof(username), stdin)` membaca username yang telah diinputkan oleh client ke dalam variabel username
- `username[strlen(username) - 1] = '\0'` menghapus newline yang ikut masuk ke dalam variabel username
- `if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }`
adalah percabangan if yang membuat socket client. Jika gagal, maka akan mencetak pesan "Socket creation error"
- `memset(&serv_addr, '0', sizeof(serv_addr))` mengosongkan struct server_addr dengan mengisinya sebagai 0
- `serv_addr.sin_family = AF_INET` mengatur family protokol alamat ke IPv4
- `serv_addr.sin_port = htons(PORT)` mengatur port server yang akan dihubungkan dengan basis variabel PORT yang sudah difenisikan sebelumnya
- `if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }`
adalah percabangan if yang digunakan untuk mengonversi alamat IP server ke format yang sesuai dengan struct serv_addr. Jika gagal, maka akan mencetak pesan "Invalid address/ Address not supported"
- `if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }`
adalah percabangan if yang menghubungkan client ke server. Jika gagal, maka akan mencetak pesan "Connection Failed"

Terdapat loop `while (true) {...}` yang digunakan untuk mengirim pesan kepada server secara terus menerus selama program tidak dimatikan. Penjelasan loop-nya adalah sebagai berikut:

- `printf("Enter your message: ")` akan mencetak pesan ke layar client agar menginputkan pesan yang akan dikirim ke server
- `fgets(message, sizeof(message), stdin)` membaca pesan yang diinputkan oleh client dan menyimpannya ke dalam variabel message
- `message[strlen(message) - 1] = '\0'` menghapus newline yang ikut masuk ke dalam variabel message
- `snprintf(fullMessage, sizeof(fullMessage), "%s : %s", username, message)` menggabungkan username dan pesan menjadi satu string dalam variabel fullMessage dengan format username : message
- `send(sock, fullMessage, strlen(fullMessage), 0)` mengirim pesan lengkap ke server melalui socket client
